[![pipeline status](https://gitlab.com/reeds_projects/conan_extern_pkg/gst-plugins-bad/badges/master/pipeline.svg)](https://gitlab.com/reeds_projects/conan_extern_pkg/gst-plugins-bad/-/commits/master)

## Conan package recipe for [*gst-plugins-bad*](https://gstreamer.freedesktop.org/modules/gst-plugins-bad.html)

GStreamer Bad Plug-ins is a set of plug-ins that aren't up to par compared to the rest. They might be close to being good quality, but they're missing something - be it a good code review, some documentation, a set of tests, a real live maintainer, or some actual wide use.

You can read more about this [here](https://gstreamer.freedesktop.org/modules/gst-plugins-bad.html).


## Issues

If you wish to report an issue or make a request for a package, please do so here:

[Issues Tracker](https://gitlab.com/groups/reeds_projects/conan_extern_pkg/-/issues)


## For Users

### Basic setup

    $ conan install gst-plugins-bad/1.16@/ --remote=gitlab

### Project setup

If you handle multiple dependencies in your project is better to add a *conanfile.txt*

    [requires]
    gst-plugins-bad/1.16@reeds_projects+conan_extern_pkg+gst-plugins-bad/release

    [generators]
    cmake

Complete the installation of requirements for your project running:

    $ mkdir build && cd build && conan install ..

Note: It is recommended that you run conan install from a build directory and not the root of the project directory.  This is because conan generates *conanbuildinfo* files specific to a single build configuration which by default comes from an autodetected default profile located in ~/.conan/profiles/default .  If you pass different build configuration options to conan install, it will generate different *conanbuildinfo* files.  Thus, they should not be added to the root of the project, nor committed to git.


## Build and package

The following command both runs all the steps of the conan file, and publishes the package to the local system cache.  This includes downloading dependencies from "build_requires" and "requires" , and then running the build() method.

    $ conan create . reeds_projects+conan_extern_pkg+gst-plugins-bad/release --build missing


### Available Options
| Option        | Default | Possible Values  |
| ------------- |:----------------- |:------------:|
| shared      | False |  [True, False] |
| fPIC      | True |  [True, False] |


## Add Remote

Conan Community has its own Bintray repository, however, we are working to distribute all package in the Conan Center:

    $ conan remote add gitlab https://gitlab.com/api/v4/packages/conan


## Conan Recipe License

NOTE: The conan recipe license applies only to the files of this recipe, which can be used to build and package mraa.
It does *not* in any way apply or is related to the actual software being packaged.

[GPL-2.0-only](LICENSE)

