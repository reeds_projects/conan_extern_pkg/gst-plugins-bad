from conans import ConanFile, tools , Meson
from conans.errors import ConanInvalidConfiguration
import glob
import os
import shutil

class gstPluginsBadConan(ConanFile):
    build_policy = "missing"
    name = "gst-plugins-bad"
    version = "1.16"
    license = "PL-2.0-only"
    description = "GStreamer Bad Plug-ins is a set of plug-ins that aren't up to par compared to the rest. They might be close to being good quality, but they're missing something - be it a good code review, some documentation, a set of tests, a real live maintainer, or some actual wide use."
    homepage = "https://gstreamer.freedesktop.org/modules/gst-plugins-bad.html"
    author = "Benoît ROPARS <benoit.ropars@solutionsreeds.fr>"
    topics = ("conan", "gst-plugins-bad", "gstreamer")
    url = "https://gitlab.com/reeds_projects/conan_extern_pkg/gst-plugins-bad"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "fPIC": [True, False]}
    default_options = {"shared": False, "fPIC": True}
    #requires = ["opencv/4.3.0@conan/stable"]
    requires = ("gstreamer/1.16.0@bincrafters/stable")
    exports = "LICENSE"
    generators = "pkg_config"
    

    def configure(self):
        del self.settings.compiler.libcxx

    def source(self):
        git = tools.Git()
        git.clone("https://gitlab.freedesktop.org/gstreamer/gst-plugins-bad.git", branch="1.16")
        git.checkout(self.version)

    def build(self):
        meson = Meson(self)
        meson.configure(defs={"cpp_std":"gnu++11"},build_folder="build")
        meson.build()

    def package_info(self):
        gst_plugin_path = os.path.join(self.package_folder, "lib", "gstreamer-1.0")
        if self.options.shared:
            self.output.info("Appending GST_PLUGIN_PATH env var : %s" % gst_plugin_path)
            self.env_info.GST_PLUGIN_PATH.append(gst_plugin_path)
        else:
            self.cpp_info.defines.append("GST_PLUGINS_BAD_STATIC")
            self.cpp_info.libdirs.append(gst_plugin_path)
            self.cpp_info.libs.extend(["gstaccurip",
            "gstadaptivedemux-1.0",
            "gstadpcmdec",
            "gstadpcmenc",
            "gstaiff",
            "gstaom",
            "gstapplemedia",
            "gstasfmux",
            "gstassrender",
            "gstaudiobuffersplit",
            "gstaudiofxbad",
            "gstaudiolatency",
            "gstaudiomixmatrix",
            "gstaudiovisualizers",
            "gstautoconvert",
            "gstbadaudio-1.0",
            "gstbasecamerabinsrc-1.0",
            "gstbayer",
            "gstbz2",
            "gstcamerabin",
            "gstclosedcaption",
            "gstcodecparsers-1.0",
            "gstcoloreffects",
            "gstcolormanagement",
            "gstcurl",
            "gstdashdemux",
            "gstde265",
            "gstdebugutilsbad",
            "gstdecklink",
            "gstdvbsuboverlay",
            "gstdvdspu",
            "gstfaac",
            "gstfaad",
            "gstfaceoverlay",
            "gstfestival",
            "gstfieldanalysis",
            "gstfreeverb",
            "gstfrei0r",
            "gstgaudieffects",
            "gstgdp",
            "gstgeometrictransform",
            "gsthls",
            "gstid3tag",
            "gstinsertbin-1.0",
            "gstinter",
            "gstinterlace",
            "gstipcpipeline",
            "gstisoff-1.0",
            "gstivfparse",
            "gstivtc",
            "gstjp2kdecimator",
            "gstjpegformat",
            "gstlegacyrawparse",
            "gstmidi",
            "gstmms",
            "gstmpegpsdemux",
            "gstmpegpsmux",
            "gstmpegts-1.0",
            "gstmpegtsdemux",
            "gstmpegtsmux",
            "gstmusepack",
            "gstmxf",
            "gstnetsim",
            "gstopencv-1.0",
            "gstopenexr",
            "gstopenjpeg",
            "gstopusparse",
            "gstpcapparse",
            "gstphotography-1.0",
            "gstplayer-1.0",
            "gstpnm",
            "gstproxy",
            "gstremovesilence",
            "gstrfbsrc",
            "gstrtponvif",
            "gstsctp-1.0",
            "gstsctp",
            "gstsdpelem",
            "gstsegmentclip",
            "gstshm",
            "gstsiren",
            "gstsmooth",
            "gstsmoothstreaming",
            "gstsndfile",
            "gstspeed",
            "gstsrt",
            "gstsrtp",
            "gstsubenc",
            "gsttimecode",
            "gstttmlsubs",
            "gsturidownloader-1.0",
            "gstvideofiltersbad",
            "gstvideoframe_audiolevel",
            "gstvideoparsersbad",
            "gstvideosignal",
            "gstvmnc",
            "gstwebp",
            "gstwebrtc-1.0",
            "gstwebrtc",
            "gstx265",
            "gsty4mdec",
            "gstyadif",
            "parser.a"])

        self.cpp_info.includedirs = ["include", os.path.join("include", "gstreamer-1.0")]

